var app = require('express').Router();
app.all('*', function(req, res, next){
  if(!req.user){
    return res.status('403').json({message: 'Authenticated user required!'});
  }
  next();
});
app.get('*', function(req, res, next){
  req.user.sendToken(function(err){
    if(err){
      res.status('500').json(err);
    } else {
      res.status('200').json({message: 'Token sent to userId '+req.user._id});
    }
  });
});
app.post('*', require('body-parser').json());
app.post('*', function(req, res, next){
  if(req.body && req.body['2fa']){
    req.user.verifyToken(req.body['2fa'], function(err, result){
      if(err) return res.status('500').json(err);

      if(result){
        return res.status('200').json({'message': 'Authenticated '+req.user.phone});
      } else {
        return res.status('401').json({'message': 'Invalid 2FA Token '});
      }

    });
  } else {
    res.status('400').json({message: "2FA Token Required"});
  }
});
exports = module.exports = app;
