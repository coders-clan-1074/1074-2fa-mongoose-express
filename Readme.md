CC#1074; Twillio SMS Verification
---

This task adds Twillio SMS account verification.

# Usage

## Set ENV variables
This libary requires the following ENV credentials;

 * TWILIO_NUMBER
 * TWILIO_SID
 * TWILIO_TOKEN

## Include UserSchema additions
Add the contents of UserSchema_ADDITIONS.js to your Mongoose UserSchema.
## Include the auth routes
In your application routes add the following code;

```javascript
var TwoFA = require('./2FA_Route');
app.use('/2fa/', TwoFA);
```

This route **must** be after a [passportjs](http://passportjs.org/) authenticated route.

## Create a client side form

### Generating a 2FA token for a user
While authenticated as a specific user, send a GET request to the route defined above.

### Authenticating a 2FA token for a user
While authenticated, send a POST request to the route defined above with the input token field name `2fa`, or JSON body as `{"2fa": "ABC1"}`.

# Checking users phone verification status
On the UserModel/UserSchema the field `verified` is a Boolean.
