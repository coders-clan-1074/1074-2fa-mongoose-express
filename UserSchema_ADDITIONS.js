var authKey = require('./lib/2FAKeyGen.js');
var twilioClient = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
UserSchema.methods.sendAuthToken = function(cb){
  var self = this;
  if(!self["2faKey"]){ self["2faKey"] = authKey(4); }
  self["2faKey"] = authKey(4);
  self.save(function(err){
    if(err)return cb(err);
    twilioClient.sendMessage({
        to: self.phone,
        from: process.env.TWILIO_NUMBER,
        body: "Your account verification code is: "+self["2faKey"]
    }, function(err, response) {
        return cb(err, response);
    });
  });

};
UserSchema.methods.verifyToken = function(token, cb){
  var verified = false;
  if(!this["2faKey"]){
    return cb(err, false);
  } else {
    if(token === this['2faKey']){
      this.verified = true;
      this.save(function(err){
        return cb(err, true);
      });
    } else {
      this.sendAuthToken(function(err){
        return cb(err, false);
      });
    }
  }
};
